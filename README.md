# node

#### Table of Contents

1. [Overview](#overview)
2. [Module Description](#module-description)
3. [Limitations](#limitations)

## Overview

Base module for installing Node on a Debian or Ubuntu system

## Module Description

Currently it installs the latest version of Node 6.x

## Limitations



