# configure an Emerging Technology Node server

class nodejs (
  $version
) {

  package { 'nodejs':
    ensure  => $version,
  }

  file { '/app':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  
  file { '/start':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "puppet:///modules/${module_name}/start"
  }
}

